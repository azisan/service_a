# Service A merupakan REST API yang dibuat menggunakan Flask framework, JWT, SQLAlchemy.
Terdapat fitur login, register, read, update, delete, dan transaksi pembelian barang untuk Service B

## Instalasi

1. Kloning repositori `$ git clone git@bitbucket.org:azisan/service_a.git`
2. Buat sebuah virtualenv untuk mengisolasi python packages dengan cara `$ virtualenv myenv`
3. Aktifkan virtualenv `$ source myenv/bin/activate`
4. Install beberapa package pendukung `$ pip install -r requirements.txt`
5. Sediakan sebuah database dengan nama misalnya `service_a` 
6. Konfigurasi database pada app.py line 20 `app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://username:password@host/service_a'`
7. Membuat tabel User pada database dengan cara masuk ke dalam python shell `$ python` kemudian `>>> from app import db` `>>> db.create_all()`
   
8. Jalankan server `$ python app.py`

## Berikut adalah beberapa endpoint yang tersedia:
- `POST` http://127.0.0.1:5001/register untuk registrasi user
`payload {"username": "example", "password": "mysecret", "saldo": 9999}`
- `POST` http://127.0.0.1:5001/login untuk login dan mendapatkan token
`payload {"username": "example", "password": "mysecret"}`
- `GET` http://127.0.0.1:5001/users untuk menampilkan semua user yang ada
- `GET` http://127.0.0.1:5001/user/`<id>` untuk menampilkan satu user
- `PUT` http://127.0.0.1:5001/user/`<id>` untuk mengupdate user
`payload {"username": "example", "password": "mysecret", "saldo": 9999}`
- `DELETE` http://127.0.0.1:5001/user/`<id>` untuk menghapus user
- `POST` http://127.0.0.1:5001/transaksi untuk Service B melakukan transaksi