from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.sql import func

import os

#Init app
app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = 'AK0X4v07X42RKxpDXsb7WhbZ68KRxZuG'
jwt = JWTManager(app)

#Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:example@172.25.0.5/service_a'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#Init db
db = SQLAlchemy(app)

#Init marshmallow
ma = Marshmallow(app)

#MODEL
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(200))
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())
    saldo = db.Column(db.Integer)

    def __init__(self, username, password, saldo):
        self.username = username
        self.password = password
        self.saldo = saldo
        
    def check_password(self, password):
        """Check hashed password."""
        return check_password_hash(self.password, password)

#User Schema
class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'username', 'password', 'created_at', 'saldo')

#Init schema
user_schema = UserSchema()
users_schema = UserSchema(many=True)

#Register
@app.route('/register', methods=['POST'])
def register():
    username = request.json['username']
    password = request.json['password']
    saldo = request.json['saldo']
    user = User.query.filter_by(username=username).first() 
    if user: 
        return jsonify({"msg": "username exists!"})
    new_user = User(username, generate_password_hash(password, method='sha256'), saldo)
    db.session.add(new_user)
    db.session.commit()
    return user_schema.jsonify(new_user)

#Login
@app.route('/login', methods=['POST'])
def login():
    username = request.json['username']
    password = request.json['password']
    user = User.query.filter_by(username=username).first() 
    if user and user.check_password(password=password):
        access_token = create_access_token(identity=username)
        return jsonify(access_token=access_token), 200
    return jsonify({"msg": "Not login"})

#Get All user
@app.route('/users', methods=['GET'])
@jwt_required
def users():
    users = User.query.all()
    result = users_schema.dump(users)
    return jsonify(result)

#Get a User
@app.route('/user/<id>', methods=['GET'])
def get_user(id):
    user = User.query.get(id)
    return user_schema.jsonify(user)

#Update a User
@app.route('/user/<id>', methods=['PUT'])
def update_user(id):
    user = User.query.get(id)
    user.username = request.json['username']
    user.password = request.json['password']
    user.saldo = request.json['saldo']
    db.session.commit()
    return user_schema.jsonify(user)

#Delete a User
@app.route('/user/<id>', methods=['DELETE'])
def delete_user(id):
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    return user_schema.jsonify(user)

#Transaksi user
@app.route('/transaksi', methods=['POST'])
def transaksi_user():
    my_lists=[]
    amount = request.json['amount']
    users = User.query.all()
    for user in users:
        if user.saldo > amount:
            user.saldo -= amount
            db.session.commit()
            my_lists.append(user)
        #else send response transaksi gagal
    result = users_schema.dump(my_lists)
    return jsonify(result)

    
if __name__ == '__main__':
    app.env="development"
    app.run(host="127.0.0.1", port=5001, debug=True)